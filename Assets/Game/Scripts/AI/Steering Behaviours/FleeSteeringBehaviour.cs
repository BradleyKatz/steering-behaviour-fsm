﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName ="ScriptableAI/Steering Behaviour/Flee")]
    public class FleeSteeringBehaviour : SteeringBehaviour
    {
        public override void Init(ActorFSMController fsmController)
        {
            FleeBehaviourData fleeData = behaviourData as FleeBehaviourData;
            fleeData.target = fsmController.seekTarget;
        }

        public override Vector3 CalculateForce(SteeringBehaviourManager steeringComponent)
        {
            if (behaviourData != null)
            {
                FleeBehaviourData fleeData = behaviourData as FleeBehaviourData;

                if (fleeData.enemyTarget == null)
                {
                    CheckMouseInput();
                }
                else
                {
                    fleeData.target = fleeData.enemyTarget.position;
                }

                float distance = (steeringComponent.transform.position - fleeData.target).magnitude;
                if (distance > fleeData.fleeDistance)
                {
                    return Vector3.zero;
                }

                Vector3 desiredVelocity = (steeringComponent.transform.position - fleeData.target).normalized;
                desiredVelocity = desiredVelocity * steeringComponent.MaxSpeed;
                return desiredVelocity - steeringComponent.Velocity;
            }
            else
            {
                Debug.LogWarning("Flee Behaviour Data is not set on Flee behaviour attached to " + steeringComponent.transform.parent.name);
                return Vector3.zero;
            }
        }
    }
}

