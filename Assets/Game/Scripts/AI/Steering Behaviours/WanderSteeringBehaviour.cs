﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName = "ScriptableAI/Steering Behaviour/Wander")]
    public class WanderSteeringBehaviour : SteeringBehaviour
    {
        public override void Init(ActorFSMController fsmController)
        {
            if (behaviourData != null)
            {
                WanderBehaviourData wanderData = behaviourData as WanderBehaviourData;

                float theta = (float)Random.value * Mathf.PI * 2;
                wanderData.WanderTarget = new Vector3(wanderData.WanderRadius * (float)Mathf.Cos(theta), wanderData.WanderRadius * (float)Mathf.Sin(theta));
            }
        }

        public override Vector3 CalculateForce(SteeringBehaviourManager steeringComponent)
        {
            if (behaviourData != null)
            {
                WanderBehaviourData wanderData = behaviourData as WanderBehaviourData;

                float jitterThisTimeSlice = wanderData.WanderJitter * Time.deltaTime;

                wanderData.WanderTarget = wanderData.WanderTarget + new Vector3(Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice, Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice);
                wanderData.WanderTarget.Normalize();
                wanderData.WanderTarget = wanderData.WanderTarget * wanderData.WanderRadius;

                wanderData.target = wanderData.WanderTarget + new Vector3(wanderData.WanderDistance, 0.0f, 0.0f);
                wanderData.target = steeringComponent.transform.rotation * wanderData.target + steeringComponent.transform.position;

                Vector3 wanderForce = wanderData.target - steeringComponent.transform.position;
                wanderForce.Normalize();
                wanderForce = wanderForce * steeringComponent.MaxSpeed;

                return wanderForce;
            }
            else
            {
                Debug.LogWarning("Wander Behaviour Data is not set on Wander behaviour attached to " + steeringComponent.transform.parent.name);
                return Vector3.zero;
            }
        }
    }
}
