﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    public abstract class SteeringBehaviour : ScriptableObject
    {
        public SteeringBehaviourData behaviourData = null;

        public virtual void Init(ActorFSMController fsmController) { }

        public abstract Vector3 CalculateForce(SteeringBehaviourManager steeringComponent);

        protected void CheckMouseInput()
        {
            if (behaviourData != null && Input.GetMouseButtonDown(0) && behaviourData.useMouseInput)
            {
                behaviourData.target = Input.mousePosition;
                behaviourData.target = Camera.main.ScreenToWorldPoint(behaviourData.target);
                behaviourData.target.z = 0.0f;
            }
        }
    }
}
