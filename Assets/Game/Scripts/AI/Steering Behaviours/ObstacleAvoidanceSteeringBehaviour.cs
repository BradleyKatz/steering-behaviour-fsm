﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName = "ScriptableAI/Steering Behaviour/Obstacle Avoidance")]
    public class ObstacleAvoidanceSteeringBehaviour : SteeringBehaviour
    {
        public override void Init(ActorFSMController fsmController)
        {
            if (behaviourData != null)
            {
                ObstacleAvoidanceBehaviourData avoidanceBehaviourData = behaviourData as ObstacleAvoidanceBehaviourData;
                avoidanceBehaviourData.maxSearchDistance = fsmController.maxSearchDistance;
            }
            else
            {
                Debug.LogWarning("Obstacle Avoidance Data not set on Obstacle Avoidance Steering Behaviour attached to " + fsmController.transform.parent.name);
            }
        }

        public override Vector3 CalculateForce(SteeringBehaviourManager steeringComponent)
        {
            if (behaviourData != null)
            {
                ObstacleAvoidanceBehaviourData avoidanceData = behaviourData as ObstacleAvoidanceBehaviourData;

                Vector3 avoidanceForce = Vector3.zero;

                // Unity sorts these by distance from the origin, no need to loop and find the closest
                RaycastHit2D[] hits = Physics2D.RaycastAll(steeringComponent.transform.position, steeringComponent.Velocity.normalized, avoidanceData.maxSearchDistance, avoidanceData.obstacleLayerMask);
                if (hits.Length > 0)
                {
                    //avoidanceForce = (steeringComponent.transform.position - hits[0].transform.position).normalized * avoidanceData.maxAvoidanceForce;
                    avoidanceForce = ((steeringComponent.Velocity.normalized * avoidanceData.maxSearchDistance) - hits[0].transform.position).normalized * avoidanceData.maxAvoidanceForce;
                }

                return avoidanceForce;
            }
            else
            {
                return Vector3.zero;
            }
        }
    }
}
