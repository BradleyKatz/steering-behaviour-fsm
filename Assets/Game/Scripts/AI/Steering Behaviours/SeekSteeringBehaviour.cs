﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName="ScriptableAI/Steering Behaviour/Seek")]
    public class SeekSteeringBehaviour : SteeringBehaviour
    {
        public override void Init(ActorFSMController fsmController)
        {
            SeekBehaviourData seekData = behaviourData as SeekBehaviourData;
            seekData.target = fsmController.seekTarget;
        }

        public override Vector3 CalculateForce(SteeringBehaviourManager steeringComponent)
        {
            if (behaviourData != null)
            {
                SeekBehaviourData seekData = behaviourData as SeekBehaviourData;

                if (seekData.useMouseInput)
                {
                    CheckMouseInput();
                }

                Vector3 desiredVelocity = (seekData.target - steeringComponent.gameObject.transform.position).normalized;
                desiredVelocity = desiredVelocity * steeringComponent.MaxSpeed;
                return desiredVelocity;
            }
            else
            {
                Debug.LogWarning("Seek Behaviour Data is not set on Wander behaviour attached to " + steeringComponent.transform.parent.name);
                return Vector3.zero;
            }
        }
    }
}
