﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName = "ScriptableAI/State")]
    public class State : ScriptableObject
    {
        public List<SteeringBehaviour> steeringBehaviours = new List<SteeringBehaviour>();
        public List<Action> actions = new List<Action>();
        public List<Transition> transitions = new List<Transition>();

        public void UpdateState(ActorFSMController fsmController)
        {
            RunActions(fsmController);
            CheckTransitions(fsmController);
        }

        private void RunActions(ActorFSMController fsmController)
        {
            foreach (Action action in actions)
            {
                action.Act(fsmController);
            }
        }

        private void CheckTransitions(ActorFSMController fsmController)
        {
            foreach (Transition transition in transitions)
            {
                bool decision = transition.decision.Decide(fsmController);

                if (decision)
                {
                    fsmController.MoveToState(transition.trueState);
                }
                else
                {
                    fsmController.MoveToState(transition.falseState);
                }
            }
        }
    }
}

