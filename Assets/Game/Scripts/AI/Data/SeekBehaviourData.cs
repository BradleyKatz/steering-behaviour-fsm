﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName ="ScriptableAI/Steering Behaviour Data/Seek Data")]
    public class SeekBehaviourData : SteeringBehaviourData
    { 
    }
}
