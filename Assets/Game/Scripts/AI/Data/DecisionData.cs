﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName = "ScriptableAI/Decision Data/Decision Data")]
    public class DecisionData : ScriptableObject
    {
        // LayerMask used for any raycasts made during the decision making process 
        public LayerMask collisionMask;
    }
}
