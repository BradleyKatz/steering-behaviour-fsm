﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    public class SteeringBehaviourData : ScriptableObject
    {
        public float weight = 1.0f;
        public Vector3 target = Vector3.zero;
        public bool useMouseInput = false;
    }
}
