﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName = "ScriptableAI/Steering Behaviour Data/Obstacle Avoidance Data")]
    public class ObstacleAvoidanceBehaviourData : SteeringBehaviourData
    {
        public float maxAvoidanceForce = 10.0f;
        public LayerMask obstacleLayerMask;

        [HideInInspector]
        public float maxSearchDistance;
    }
}
