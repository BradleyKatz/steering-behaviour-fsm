﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName ="ScriptableAI/Steering Behaviour Data/Flee Data")]
    public class FleeBehaviourData : SteeringBehaviourData
    {
        public Transform enemyTarget = null;
        public float fleeDistance = 1.0f;
    }
}
