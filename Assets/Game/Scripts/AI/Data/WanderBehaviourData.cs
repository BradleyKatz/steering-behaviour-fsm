﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName="ScriptableAI/Steering Behaviour Data/Wander Data")]
    public class WanderBehaviourData : SteeringBehaviourData
    {
        public float WanderDistance = 2.0f;
        public float WanderRadius = 1.0f;
        public float WanderJitter = 20.0f;

        [HideInInspector]
        public Vector3 WanderTarget = Vector3.zero;
    }
}