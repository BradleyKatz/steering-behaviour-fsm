﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [CreateAssetMenu(menuName = "ScriptableAI/Decisions/Search Decision")]
    public class SearchDecision : Decision
    {
        public override bool Decide(ActorFSMController fsmController)
        {
            return IsTargetInSight(fsmController);
        }

        private bool IsTargetInSight(ActorFSMController fsmController)
        {
            if (decisionData == null)
            {
                Debug.LogWarning("Decision Data not set on Decision");
                return false;
            }

            RaycastHit2D hit = Physics2D.CircleCast(fsmController.transform.position, fsmController.sphereCastSearchRadius, fsmController.transform.forward, fsmController.maxSearchDistance, decisionData.collisionMask);
            if (hit.transform != null)
            {
                fsmController.seekTarget = hit.transform.position;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
