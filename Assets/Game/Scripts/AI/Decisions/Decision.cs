﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    public abstract class Decision : ScriptableObject
    {
        public DecisionData decisionData = null;

        public abstract bool Decide(ActorFSMController fsmController);
    }
}
