﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PluggableAI
{
    [RequireComponent(typeof(SteeringBehaviourManager))]
    public class ActorFSMController : MonoBehaviour
    {
        private SteeringBehaviourManager steeringManager;

        public State currentState = null;
        public float sphereCastSearchRadius = 10.0f;
        public float maxSearchDistance = 20.0f;

        [HideInInspector]
        public Vector3 seekTarget = Vector3.zero;

        private void Start()
        {
            steeringManager = GetComponent<SteeringBehaviourManager>();

            if (currentState != null)
            {
                steeringManager.SetCurrentState(currentState);
            }
        }

        private void Update()
        {
            if (currentState != null)
            {
                currentState.UpdateState(this);
            }
        }

        public void MoveToState(State nextState)
        {
            if (currentState != nextState)
            {
                currentState = nextState;
                steeringManager.SetCurrentState(currentState);

                foreach (SteeringBehaviour steeringBehaviour in currentState.steeringBehaviours)
                {
                    steeringBehaviour.Init(this);
                }
            }
        }
    }
}
